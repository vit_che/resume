-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Авг 14 2020 г., 18:53
-- Версия сервера: 5.7.25
-- Версия PHP: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `new-res`
--

-- --------------------------------------------------------

--
-- Структура таблицы `aboutmes`
--

CREATE TABLE `aboutmes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `aboutmes`
--

INSERT INTO `aboutmes` (`id`, `name`, `info`, `status`, `created_at`, `updated_at`) VALUES
(1, 'first', 'Hi! I\'m Vitalii Chebotnikov. I\'m a web-developer.\r\nI started as a QA Automation tester. \r\n\r\nI started to learn the JAVA myself and finished the QA Automation courses (got a certificate) because I didn\'t have a  manual testing background I couldn\'t get QA Automation position. Like a freelance tested I had tested two IT-projects.  I continued my self web-education and on the courses of the  IT Step academy (web-development, 1 year). While I was studying I made my first little commercial project (a little corporate application). \r\n<br>\r\nMy first full-time job was a position in the Managerigs.com. \r\nIn that company, I  was developing admin-panel and background functionality, the main site, and user cabinet.\r\n\r\nMy next full-time job was a position in the Mementia company.\r\nMy main work was customization and support existing e-commerce projects. I worked with Symfony/Prestashop/Shopify/Magento2/Odoo  projects. Almost two years I am working out stuff on a Symfony project\r\n<br>\r\nSince 12.2019 I work a little part-time in the Mementia on the Symfony project and work as a freelancer. \r\nI can\'t show these projects in my portfolio cause NDA. But I can show my little training projects in the \"My APPs\"\r\n<br>\r\n About stacks that I use.\r\nMainly I use PHP, a little JS/JQuery & sometimes Python. I can execute not difficult tasks on the front-end but I like to work with back-end more.  I can work with Rest API,  I know Docker a little. I work with Composer / Git / Jira. I can create REST API. \r\n<br>\r\nThis site I created myself on the AWS (Linux / Apache / PHP / MySQL). I used Laravel & Bootstrap & a little JS.All content adds through Admin panel. I can solve many tasks on my own.', 1, NULL, '2020-08-14 04:15:08'),
(2, 'second', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, NULL, '2020-08-12 07:29:08');

-- --------------------------------------------------------

--
-- Структура таблицы `avatars`
--

CREATE TABLE `avatars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `avatars`
--

INSERT INTO `avatars` (`id`, `filename`, `path`, `status`, `created_at`, `updated_at`) VALUES
(6, 'data_dude.svg', 'uploads/avatars/Xjgojx4sOPG2ahwobonFA2qfyko5B9p6Q64zDZT8.svg', 0, '2020-08-09 09:38:30', '2020-08-14 06:50:18'),
(8, 'smile_dude.svg', 'uploads/avatars/MYbfcG2kg79HYDP3vd9bTdy78lKxTWWWGfj8cNuA.svg', 1, '2020-08-09 09:38:45', '2020-08-14 06:50:18'),
(9, 'default.svg', 'uploads/avatars/gKbam9U8UzaMA0kKu4fGEs75byR6X02Ny9iAdm0t.svg', 0, '2020-08-09 10:03:26', '2020-08-14 05:27:27'),
(11, 'jedi_dude.svg', 'uploads/avatars/JW1BJtKOdQGu0hSvBX257y9BUVlpsYCeAulC4BEO.svg', 0, '2020-08-11 07:28:14', '2020-08-11 07:28:14');

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `images`
--

INSERT INTO `images` (`id`, `filename`, `path`, `status`, `created_at`, `updated_at`) VALUES
(1, 'code.png', 'uploads/images/UxVR4LyJ8SMleTK5whGDucEVSgmdph6BqR34LHRd.png', 0, '2020-08-11 06:56:21', '2020-08-14 05:27:05'),
(2, 'alter.png', 'uploads/images/TA1TEyuROIqE3l7Ttj4PqDlJtVmgf6yuaOWOjedi.png', 0, '2020-08-11 07:42:33', '2020-08-14 06:50:40'),
(3, 'back.png', 'uploads/images/8o3TqiaW8MoqypXtvXKIUCcUPTUe9xU3S5MA7IBc.png', 1, '2020-08-12 04:45:27', '2020-08-14 06:50:40');

-- --------------------------------------------------------

--
-- Структура таблицы `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `dateRange` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `companyName` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `typeJob` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `library` text COLLATE utf8mb4_unicode_ci,
  `langs` text COLLATE utf8mb4_unicode_ci,
  `stack` text COLLATE utf8mb4_unicode_ci,
  `additions` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `companyLink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `jobs`
--

INSERT INTO `jobs` (`id`, `dateRange`, `companyName`, `typeJob`, `position`, `library`, `langs`, `stack`, `additions`, `companyLink`, `created_at`, `updated_at`) VALUES
(1, '09.2017 - 05.2018', 'Freelance', 'freelance projects', 'QA manual', NULL, NULL, NULL, 'manual testing', NULL, '2020-08-11 15:06:35', '2020-08-11 15:06:35'),
(2, '05.2018 - 11.2018', 'Managerigs.com', 'full-time', 'web developer', 'Bootstrap', 'PHP, JS, Python', 'HTML/CSS/Bootstrap/MySQL/', 'Worked with main site, created client cabinet, customized admin panel, created chat bot', 'https://managerigs.com/', '2020-08-06 16:30:59', '2020-08-06 16:30:59'),
(3, '12.2018 - 12.2020', 'Mementia.com', 'full-time!', 'web developer', 'Symfony/Prestashop/Shopify/Magento2/Odoo', 'PHP, JS, Python', 'HTML/CSS/Bootstrap/MySQL/', 'Support & customization e-commerce projects', 'https://mementia.com/', '2020-08-06 16:34:13', '2020-08-10 15:50:34'),
(4, '12.2019 - current time', 'Mementia.com', 'part-time', 'web developer', 'Symfony', 'PHP, JS', 'HTML/CSS/Bootstrap/MySQL/JQuery/Git', 'customization e-commerce Symfony project', 'https://mementia.com/', '2020-08-10 15:35:10', '2020-08-11 13:30:42'),
(5, '12.2019 - current time', 'freelance', 'part-time', 'web developer', 'Laravel', 'PHP, JS', 'HTML/CSS/Bootstrap/MySQL/JQuery/Git', 'work with small Laravel projects', NULL, '2020-08-10 15:36:39', '2020-08-10 15:53:15');

-- --------------------------------------------------------

--
-- Структура таблицы `letters`
--

CREATE TABLE `letters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `letters`
--

INSERT INTO `letters` (`id`, `filename`, `path`, `status`, `created_at`, `updated_at`) VALUES
(8, 'CV_Chebotnikovi.rtf', 'uploads/letters/CV_Chebotnikovi.rtf', 1, '2020-08-12 03:43:44', '2020-08-12 04:20:22'),
(9, 'CV_Chebotnikovi_2020.rtf', 'uploads/letters/CV_Chebotnikovi_2020.rtf', 0, '2020-08-12 04:19:38', '2020-08-12 04:19:38'),
(10, 'CV_Chebotnikov-Vitalii_Djinni.docx', 'uploads/letters/CV_Chebotnikov-Vitalii_Djinni.docx', 0, '2020-08-12 04:21:22', '2020-08-12 04:21:22');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 2),
(4, '2020_08_06_144612_create_jobs_table', 3),
(5, '2020_08_08_153211_create_avatars_table', 3),
(7, '2020_08_11_060335_create_aboutmes_table', 4),
(8, '2020_08_11_093250_create_images_table', 5),
(9, '2020_08_11_202042_create_letters_table', 6),
(10, '2020_08_13_091939_create_projects_table', 7);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` char(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'http://new-res/',
  `git_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skills` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `projects`
--

INSERT INTO `projects` (`id`, `name`, `link`, `git_link`, `skills`, `description`, `img_name`, `img_path`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Currency change calculator', 'http://mycur/', 'https://github.com/dokermast/curency', 'HTML / CSS / Bootstrap / JS / JQuery / Ajax /PHP / MySQL', 'The application made without using frameworks.\r\nThe currency rates  uploads from free open API source http://www.floatrates.com/daily/usd.json.\r\nUser can select currencies from list and how many rows should be displayed in the History table.', 'currency.jpg', 'uploads/images/currency.jpg', 1, '2020-08-13 07:16:55', '2020-08-13 13:31:31'),
(3, 'second Calc', 'http://mycur/', NULL, 'asdasdasda', 'sdasdasdasd', 'currency.jpg', 'uploads/images/currency.jpg', 0, '2020-08-13 12:31:45', '2020-08-14 06:53:34'),
(4, 'Third Calc', 'http://mycur/', 'https://github.com/dokermast/curency', 'dasdasdas', 'asdasdasdas', 'velo.jpg', 'uploads/images/velo.jpg', 0, '2020-08-13 13:07:15', '2020-08-14 06:53:39');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@example.com', NULL, '$2y$10$D8HNr6.1BZGctJP8.i2ZcuJLdjMT5aJ5GSTDl6dyOkMWqJYLUUVkS', NULL, '2020-08-06 09:24:48', '2020-08-06 09:24:48');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `aboutmes`
--
ALTER TABLE `aboutmes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `avatars`
--
ALTER TABLE `avatars`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `letters`
--
ALTER TABLE `letters`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `aboutmes`
--
ALTER TABLE `aboutmes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `avatars`
--
ALTER TABLE `avatars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `letters`
--
ALTER TABLE `letters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
