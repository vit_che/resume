<?php

namespace App\Http\Controllers;

use App\Aboutme;
use App\Letter;
use App\Avatar;
use App\Image;
use App\Project;
use App\Visitor;
use App\Http\Location;
use Illuminate\Http\Request;
use App\Job;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Cache;

class MainController extends Controller
{
    public function index(Request $request)
    {
        $ip = $request->getClientIp();
        $visited_date = Date("Y-m-d:H:i:s");

        $location = Location::locationGet($ip);
        $location = ($location) ? $location['country_name'] . " : " . $location['city'] : "noname";
        $visitor = Visitor::updateOrCreate(['ip' => $ip, 'location' => $location], ['visited_date' => $visited_date]);
        $visitor->increment('hits');

        $jobs = Job::all();
        $avatar = Avatar::where('status', 1)->first();
        $aboutme = Aboutme::where('status', 1)->first();
        $image = Image::where('status', 1)->first();
        $letter = Letter::where('status', 1)->first();
        $projects = Project::where('status', 1)->get();

        return view('sections.content', compact('jobs'), ['avatar' => $avatar, 'aboutme' => $aboutme, 'image' =>$image, 'letter' => $letter, 'projects' => $projects]);
    }

    public function form()
    {
        return view('form');
    }


//    public function contact(Request $request)
//    {
//        if ($request->isMethod('post')) {
//
//            $input = $request->except('_token');
//
//            try {
//                Mail::send('layouts.mail', ['input' => $input], function ($message) {
//                    $message->from('doker42@gmail.com');
//                    $message->to('doker42@gmail.com', '');
//                });
//
//                echo 'OK';
//
//            } catch (\Exception $e){
//
//                echo($e->getMessage());
//            }
//        }
//
//        return view('form');
//    }

}
